import Avatar from '@atlaskit/avatar';

import OldAppSwitcherIcon from '@atlaskit/icon/glyph/menu';
import OldArrowRightLongIcon from '@atlaskit/icon/glyph/arrow-right';
import OldCancelIcon from '@atlaskit/icon/glyph/cross';
import OldConfirmIcon from '@atlaskit/icon/glyph/check';
import OldExpandIcon from '@atlaskit/icon/glyph/chevron-down';
import OldHelpIcon from '@atlaskit/icon/glyph/question-circle'; // trailing comment
import OldHintIcon from '@atlaskit/icon/glyph/lightbulb-filled';
import OldMediaUploaderIcon from '@atlaskit/icon/glyph/upload';
import OldLogInIcon from '@atlaskit/icon/glyph/sign-in';
import OldSquareIcon from '@atlaskit/icon/glyph/media-services/rectangle';
import OldRemoveIcon from '@atlaskit/icon/glyph/trash';
import OldScheduleAddIcon from '@atlaskit/icon/glyph/schedule';
import OldTimeIcon from '@atlaskit/icon/glyph/recent';

const myOtherVar = true;
