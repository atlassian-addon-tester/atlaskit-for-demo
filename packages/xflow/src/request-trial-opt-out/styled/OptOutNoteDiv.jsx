import styled from 'styled-components';

const OptOutNoteDiv = styled.div`
  width: 368px;
`;

OptOutNoteDiv.displayName = 'OptOutNoteDiv';
export default OptOutNoteDiv;
