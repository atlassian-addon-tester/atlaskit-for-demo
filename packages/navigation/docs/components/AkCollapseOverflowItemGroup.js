import React from 'react';

export const description = (
  <div>
    <p>
      This component is part of the overflow to dropdown feature of the collapsed navigation.
    </p>
    <p>
      It must be nested inside an AkCollapseOverflow component.
    </p>
    <p>
      Remember to set the required numeric props: overflowGroupIndex and itemCount. Your first
      AkCollapseOverflowItemGroup should have overflowGroupIndex set to 0.
    </p>
  </div>
);

export const examples = [];

export const byline = 'Wraps your AkNavigationItemGroup for the "overflow to dropdown" feature.';
