# @atlaskit/code

## 2.2.0 (2017-10-27)

* feature; support language=text for atlaskit/code component (issues closed: ed-2861) ([6fe1ac3](https://bitbucket.org/atlassian/atlaskit/commits/6fe1ac3))
## 2.1.4 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))





## 2.1.3 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.1.2 (2017-06-21)


* fix; fixes a bug where code block would render without content on high-res monitors ([00dee6f](https://bitbucket.org/atlassian/atlaskit/commits/00dee6f))

## 2.1.1 (2017-05-30)


* fix; tweaking styles, setting html on copy ([4bbc328](https://bitbucket.org/atlassian/atlaskit/commits/4bbc328))

## 2.1.0 (2017-05-26)


* feature; adding code/codeBlock support to renderer/editor-hipchat ([bc9d6bf](https://bitbucket.org/atlassian/atlaskit/commits/bc9d6bf))

## 2.0.1 (2017-05-23)


* fix; update dependencies ([85068ba](https://bitbucket.org/atlassian/atlaskit/commits/85068ba))

## 1.0.0 (2017-05-01)


* feature; adding code component ([171b5a7](https://bitbucket.org/atlassian/atlaskit/commits/171b5a7))
